package com.github.andrdev.bestclockever;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ClockView extends View {

    Point[] arrSeconds = new Point[60];
    Point[] arrMinutes = new Point[60];
    Point[] arrHours = new Point[60];

    Paint clockPaint;
    Paint secondsPaint;
    Paint minutesPaint;
    Paint hoursPaint;
    Paint digitsPaint;

    int diameter;
    int textSize;

    public ClockView(Context context) {
        super(context);
        clockinit();
    }

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        clockinit();
    }

    public ClockView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        clockinit();
    }

    private void clockinit() {
        textSize = 40;
        clockPaint = new Paint();
        clockPaint.setColor(Color.LTGRAY);
        clockPaint.setAntiAlias(true);

        secondsPaint = getSecondsPaint();

        minutesPaint = getMinutesPaint();

        hoursPaint = getHoursPaint();

        digitsPaint = getDigitsPaint();

        startScheduler();
    }

    private void startScheduler() {
        ScheduledExecutorService scheduleTaskExecutor = Executors.newSingleThreadScheduledExecutor();
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                post(new Runnable() {
                    @Override
                    public void run() {
                        invalidate();
                    }
                });
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    private Paint getDigitsPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        paint.setTextSize(textSize);
        return paint;
    }

    private Paint getHoursPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.GRAY);
        paint.setStrokeWidth(8);
        paint.setAntiAlias(true);
        return paint;
    }

    private Paint getMinutesPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(6);
        paint.setAntiAlias(true);
        return paint;
    }

    private Paint getSecondsPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(4);
        paint.setAntiAlias(true);
        return paint;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        diameter = Math.min(w, h);
        createBaseClock();
    }

    private void createBaseClock() {
        int radius = diameter/2;

        int secondsHandLength = (int)(radius * 0.9);
        int minutesHandLength = (int)(radius * 0.7);
        int hourHandLength = (int)(radius * 0.5);

        double radSec = Math.PI * 2 / 60;

        for (int i = 1; i < 16; i++) {
            double cos = Math.cos(radSec * i);
            double sin = Math.sin(radSec * i);
            int xSecond = (int)Math.ceil(cos * secondsHandLength + radius);
            int ySecond = (int)Math.ceil((sin * secondsHandLength + radius - diameter)*-1);

            int xMinutes = (int)Math.ceil(cos * minutesHandLength + radius);
            int yMinutes = (int)Math.ceil((sin * minutesHandLength + radius - diameter)*-1);

            int xHours = (int)Math.ceil(cos * hourHandLength + radius);
            int yHours = (int)Math.ceil((sin * hourHandLength + radius - diameter)*-1);

            int c = (i-15) * -1;
            arrSeconds[c] = new Point(xSecond, ySecond);
            arrMinutes[c] = new Point(xMinutes, yMinutes);
            arrHours[c] = new Point(xHours, yHours);
        }
        for (int i = 15; i < 30; i++) {
            Point temp = arrSeconds[i%15];
            arrSeconds[i]= new Point(diameter - temp.getyPoint(), temp.getxPoint());

            temp = arrMinutes[i%15];
            arrMinutes[i]= new Point(diameter - temp.getyPoint(), temp.getxPoint());

            temp = arrHours[i%15];
            arrHours[i]= new Point(diameter - temp.getyPoint(), temp.getxPoint());
        }
        for (int i = 30; i < 45; i++) {
            Point temp = arrSeconds[i%15];
            arrSeconds[i]= new Point(-temp.getxPoint() + diameter, -temp.getyPoint() + diameter);

            temp = arrMinutes[i%15];
            arrMinutes[i]= new Point(-temp.getxPoint() + diameter, -temp.getyPoint() + diameter);

            temp = arrHours[i%15];
            arrHours[i]= new Point(-temp.getxPoint() + diameter, -temp.getyPoint() + diameter);
        }
        for (int i = 45; i < 60; i++) {
            Point temp = arrSeconds[i%15];
            arrSeconds[i]= new Point(temp.getyPoint(), diameter - temp.getxPoint());

            temp = arrMinutes[i%15];
            arrMinutes[i] = new Point(temp.getyPoint(), diameter - temp.getxPoint());

            temp = arrHours[i%15];
            arrHours[i]= new Point(temp.getyPoint(), diameter - temp.getxPoint());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawClockCircle(canvas);

        Point secondsPoint = arrSeconds[getSecond()];
        canvas.drawLine(diameter / 2, diameter / 2,
                secondsPoint.getxPoint(), secondsPoint.getyPoint(), secondsPaint);

        int minutes = getMinute();
        Point minutesPoint = arrMinutes[minutes];
        canvas.drawLine(diameter / 2, diameter / 2,
                minutesPoint.getxPoint(), minutesPoint.getyPoint(), minutesPaint);

        int hourPos = getHour()*5 + minutes / 12;
        Log.d("dreesHour", "onDraw: "+ hourPos);
        Point hoursPoint = arrHours[hourPos];
        canvas.drawLine(diameter / 2, diameter / 2,
                hoursPoint.getxPoint(), hoursPoint.getyPoint(), hoursPaint);
    }

    private void drawClockCircle(Canvas canvas) {
        canvas.drawCircle(diameter / 2, diameter / 2, diameter / 2, clockPaint);
        canvas.drawText("12", diameter / 2 - textSize / 2, textSize, digitsPaint);
        canvas.drawText("3", diameter - textSize , diameter / 2 + textSize / 4, digitsPaint);
        canvas.drawText("6", diameter / 2 - textSize / 4, diameter - textSize / 4, digitsPaint);
        canvas.drawText("9", 0 + textSize / 4, diameter / 2 + textSize / 4, digitsPaint);
    }

    private int getHour() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh");
        int h = Integer.parseInt(sdf.format(new Date()));
        return h%12;
    }

    private int getMinute() {
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        return Integer.parseInt(sdf.format(new Date()));
    }

    private int getSecond() {
        SimpleDateFormat sdf = new SimpleDateFormat("ss");
        return Integer.parseInt(sdf.format(new Date()));
    }
}
